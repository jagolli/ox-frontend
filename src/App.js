import './App.css';
import React, { Component } from "react";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import Layout from './containers/Layout';

function App() {


    const darkTheme = createMuiTheme({
        palette: {
            type: 'dark',
        },
    });

    return (
        <ThemeProvider theme={darkTheme}>
        <Layout></Layout>
        </ThemeProvider>
    );

}

export default App;