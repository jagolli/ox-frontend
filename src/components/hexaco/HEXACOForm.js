import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import IntroPage from "./IntroPage";
import QuestionsPage from "./QuestionsPage";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 750,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
}));

var steps = ['Intro'];

//This will be a GET request from Axios
const questions = [
    {id: 0, text: 'I would be quite bored by a visit to an art gallery.', score: ''},
    {id: 1, text: 'I clean my office or home quite frequently.', score: ''},
    {id: 2, text: 'I rarely hold a grudge, even against people who have badly wronged me.', score: ''},
    {id: 3, text: 'I feel reasonably satisfied with myself overall.', score: ''},
    {id: 4, text: 'I would feel afraid if I had to travel in bad weather conditions.', score: ''},
];

steps = steps.concat(questions.map(question => question.id));

function getStepContent(step, setActiveStep, question, setQuestion, pressed, setPressed) {
    if (step === 0) {
        return <IntroPage/>;
    } else {
        return <QuestionsPage
            activeStep={step}
            setActiveStep={setActiveStep}
            question={question}
            setQuestion={setQuestion}
            questions={questions}
            pressed={pressed}
            setPressed={setPressed}
        />;
    }
}

export default function HEXACOForm() {

    const classes = useStyles();

    const [activeStep, setActiveStep] = React.useState(0);
    const [question, setQuestion] = React.useState(questions[0]);
    const [pressed,setPressed] = React.useState(false);

    const handleNext = () => {
        setActiveStep(activeStep + 1);
        setQuestion(questions[activeStep]);

    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
        setQuestion(questions[activeStep-2]);
    };


    return (
        <React.Fragment>
            <CssBaseline/>
                <React.Fragment>

                    {activeStep === steps.length ? (
                        <React.Fragment>
                            <Typography variant="subtitle1">
                                Your submission number is #2001539, test has been saved.
                            </Typography>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            {getStepContent(activeStep,setActiveStep,question,setQuestion,pressed,setPressed)}
                            <div className={classes.buttons}>
                                {activeStep !== 0 && (
                                    <Button onClick={handleBack} className={classes.button}>
                                        Back
                                    </Button>
                                )}
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={handleNext}
                                    className={classes.button}
                                >
                                    {activeStep === steps.length - 1 ? 'Submit' : 'Next'}
                                </Button>
                            </div>
                        </React.Fragment>
                    )}
                </React.Fragment>

        </React.Fragment>
    );
}

