import React from "react";
import Pagination from '@material-ui/lab/Pagination';
import {makeStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Question from "./Question";


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    pagination: {
        marginTop: theme.spacing(2),
    },
}));

export default function QuestionPage(props) {

    const classes = useStyles();
    const questions = props.questions;
    const [question, setQuestion] = [props.question, props.setQuestion];
    const [activeStep, setActiveStep]= [props.activeStep, props.setActiveStep];
    const [pressed,setPressed] = [props.pressed, props.setPressed];

    const handlePageChange = (event, page) => {
        setActiveStep(page);
        setQuestion(questions[page-1]);
    };

    return (
        <React.Fragment>
            <Grid container className={classes.root} justify="center">
                <Grid item align="center" xs={12} md={12}>
                    <Question question={question} pressed={pressed} setPressed={setPressed}/>
                </Grid>
                <div className={classes.pagination}>
                    <Pagination
                        count={questions.length}
                        size="small"
                        page={activeStep}
                        onChange={handlePageChange}
                    />
                </div>
            </Grid>
        </React.Fragment>
    );
}
