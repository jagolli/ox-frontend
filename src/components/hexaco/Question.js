import React from "react";
import {Typography} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from '@material-ui/core/Radio';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    options: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    labels: {
        display: 'flex',
    },
}));

export default function Question(props) {

    const classes = useStyles();

    const question = props.question;
    const [pressed, setPressed] = [props.pressed, props.setPressed];
    const [value, setValue] = React.useState("3");

    const handleChange = (event) => {

        setValue(event.target.value);
        setPressed(true);

        if (pressed === true) {

            setPressed(false);


        }


    };

    return (
        <React.Fragment>
            <div className={classes.root}>
                <Typography variant="body1">
                    {question.text}
                </Typography>
            </div>
            <Grid className={classes.options} item align="center" xs={12}>
                <FormControl component="fieldset">
                    <RadioGroup className={classes.labels}
                                onChange={handleChange}
                                value={value}
                                row aria-label="position"
                                name="position"
                                defaultValue="3">
                        <FormControlLabel
                            value={"1"}
                            control={<Radio color="primary"/>}
                            label="Strongly Disagree"
                            labelPlacement="bottom"
                        />
                        <FormControlLabel
                            value={"2"}
                            control={<Radio color="primary"/>}
                            label="Disagree"
                            labelPlacement="bottom"
                        />
                        <FormControlLabel
                            value={"3"}
                            control={<Radio color="primary"/>}
                            label="Neutral"
                            labelPlacement="bottom"
                        />
                        <FormControlLabel
                            value={"4"}
                            control={<Radio color="primary"/>}
                            label="Agree"
                            labelPlacement="bottom"
                        />
                        <FormControlLabel
                            value={"5"}
                            control={<Radio color="primary"/>}
                            label="Strongly Agree"
                            labelPlacement="bottom"
                        />
                    </RadioGroup>
                </FormControl>
            </Grid>
        </React.Fragment>
    );
}
