import React from "react";
import {Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

export default function IntroPage() {

    return (
        <React.Fragment>
            <Grid container xs={12}>
                <Typography>
                    The HEXACO Personality Inventory-Revised is an instrument that assesses the six major dimensions of
                    personality:
                </Typography>
                <ul>
                    <li>Honesty-Humility</li>
                    <li>Emotionality</li>
                    <li>eXtraversion</li>
                    <li>Agreeableness (versus Anger)</li>
                    <li>Conscientiousness</li>
                    <li>Openness to Experience</li>
                </ul>
            </Grid>
        </React.Fragment>
    );
}

