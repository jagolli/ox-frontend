import React, {useEffect, useState} from 'react';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import 'date-fns';
import {Field, FieldArray} from 'formik';
import {DateTimePicker} from 'formik-material-ui-pickers';
import {TextField} from 'formik-material-ui';
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import Chip from "@material-ui/core/Chip";
import SchoolIcon from "@material-ui/icons/School";
import Divider from "@material-ui/core/Divider";
import axios from "axios";
import TextFieldOriginal from "@material-ui/core/TextField";
import Autocomplete from '@material-ui/lab/Autocomplete';
import UserDetails from "./UserDetails";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 300,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    GPA: {
        width: 110,
    },
    margin: {
        margin: theme.spacing(1),
    },
    divider: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        width: '100%',
    },
    section: {
        marginTop: theme.spacing(2),
    }
}));

export default function EducationExperience(props) {

    const classes = useStyles();
    const {values, errors, states, setStates, countries, cities, setCities, setFieldValue, setValues} = {...props};
    const [idx, setIdx] = useState(0);
    const [universities, setUniversities] = useState([{}]);
    const [majors, setMajors] = useState([{}]);

    const [values1, setValues1] = useState({
        startDate: '',
        endDate: '',
    })

    async function getUniversities() {
        let response = await axios.get('http://127.0.0.1:8000/universities/');
        let data = await response.data;
        setUniversities(data);
    }

    async function getMajors() {
        let response = await axios.get('http://127.0.0.1:8000/majors/');
        let data = await response.data;
        setMajors(data);
    }

    async function getCities() {
        let response = await axios.get('http://127.0.0.1:8000/cities/?country__name='+ values.educations[idx].country + '&region__name=' + values.educations[idx].state);
        let data = await response.data;
        setCities(data);
    }

    async function getStates() {
        let response = await axios.get('http://127.0.0.1:8000/states/?country__name=' + values.educations[idx].country);
        let data = await response.data;
        setStates(data);
    }


    useEffect(() => {
        getUniversities();
        getMajors();
        getCities();
        getStates();
    }, [values]);

    const handleStartDateChange = (date) => {
        setValues1({...values, ["startDate"]: date});
    };

    const handleEndDateChange = (date) => {
        setValues1({...values, ["endDate"]: date});
    };


    // <Chip icon={<SchoolIcon/>} label="Education 1"/>
    return (
        <React.Fragment>
            <FieldArray
                name="educations"
                render={arrayHelpers => (
                    <div className={classes.section}>
                        {values.educations.map((education, index) => (
                            <Grid container spacing={3}>
                                {index > 0 &&
                                <Divider className={classes.divider}/>
                                }
                                {index > 0 &&
                                <Grid item md={12}>
                                    <Chip icon={<SchoolIcon/>} label={"Education " + (index + 1)}/>
                                </Grid>
                                }
                                {setIdx(index)}
                                <Grid item container justify="center" sm={4} md={4}>
                                    <div key={index} className={classes.formControl}>
                                        <Field
                                            name={`educations.${index}.level`}
                                            component={TextField}
                                            select
                                            fullWidth
                                            label={'Education level'}
                                        >
                                            <MenuItem value="">
                                                <em>Select an option</em>
                                            </MenuItem>
                                            <MenuItem value="None">None</MenuItem>
                                            <MenuItem value="High School Diploma">High School Diploma</MenuItem>
                                            <MenuItem value="Associate">Associate</MenuItem>
                                            <MenuItem value="Bachelor's">Bachelor's</MenuItem>
                                            <MenuItem value="Masters">Masters</MenuItem>
                                            <MenuItem value="Doctorate">Doctorate</MenuItem>
                                        </Field>
                                    </div>
                                </Grid>
                                <Grid item container justify="center" sm={2} md={2}>
                                    <Field
                                        component={TextField}
                                        className={classes.GPA}
                                        name={`educations[${index}].gpa`}
                                        label="GPA"
                                        type="number"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">/ 4.0</InputAdornment>,
                                        }}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid item container justify="center" sm={6} md={6}>
                                    <Autocomplete
                                        freeSolo
                                        name={`educations.${index}.fieldOfStudy`}
                                        value={values.educations[index].fieldOfStudy}
                                        onChange={(e, value) =>
                                            setFieldValue(`educations.${index}.fieldOfStudy`, value)
                                        }
                                        options={majors.map((option) => option.name)}
                                        fullWidth
                                        renderInput={(params) => (
                                            <TextFieldOriginal
                                                {...params}
                                                errors={''}
                                                helperText={''}
                                                name={`educations.${index}.fieldOfStudy`}
                                                label="Field of Study"
                                            />
                                        )}
                                    />
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                    <Autocomplete
                                        freeSolo
                                        name={`educations.${index}.school`}
                                        value={values.educations[index].school}
                                        onChange={(e, value) =>
                                            setFieldValue(`educations.${index}.school`, value)}
                                        options={universities.map(university => university.name)}
                                        fullWidth
                                        renderInput={(params) => (
                                            <TextFieldOriginal
                                                {...params}
                                                errors={''}
                                                helperText={''}
                                                name={`educations.${index}.school`}
                                                label="School"
                                            />
                                        )}
                                    />
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                    <Autocomplete
                                        name={`educations.${index}.country`}
                                        options={countries.map(country => country.name)}
                                        value={values.educations[index].country}
                                        onChange={(e, value) =>
                                            setFieldValue(`educations.${index}.country`, value) &&
                                            setFieldValue(`educations.${index}.state`, '') &&
                                            setFieldValue(`educations.${index}.city`, '')
                                        }
                                        fullWidth
                                        autoComplete="shipping country"
                                        renderInput={(params) => (
                                            <TextFieldOriginal
                                                {...params}
                                                error={errors['country']}
                                                helperText={errors['country']}
                                                name={`educations.${index}.country`}
                                                label="Country"
                                                autoComplete="shipping country"
                                            />
                                        )}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <Autocomplete
                                        name={`educations.${index}.state`}
                                        options={states.map(state => state.name)}
                                        value={values.educations[index].state}
                                        onChange={(e, value) =>
                                            setFieldValue(`educations.${index}.state`, value) &&
                                            setFieldValue(`educations.${index}.city`, '')
                                        }
                                        fullWidth
                                        renderInput={(params) => (
                                            <TextFieldOriginal
                                                {...params}
                                                error={''}
                                                helperText={''}
                                                name={`educations.${index}.state`}
                                                label="State/Province/Region"
                                            />
                                        )}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <Field
                                        name={`educations.${index}.city`}
                                        component={Autocomplete}
                                        options={cities.map(city => city.name)}
                                        // options={countries}
                                        fullWidth
                                        autoComplete="shipping address-level2"
                                        getOptionLabel={(option) => option}
                                        renderInput={(params) => (
                                            <TextFieldOriginal
                                                {...params}
                                                error={errors[`educations.${index}.city`]}
                                                helperText={errors[`educations.${index}.city`]}
                                                label="City"
                                            />
                                        )}
                                    />
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                    <Field
                                        component={TextField}
                                        name={`educations.${index}.zip`}
                                        label="Zip / Postal code"
                                        fullWidth
                                        autoComplete="shipping postal-code"
                                    />
                                </Grid>
                                <Grid item xs={6} sm={6}/>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <Grid justify="flex-end" item container xs={5}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="startDate"
                                            label="Start date"
                                            value={values.startDate}
                                            onChange={handleStartDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                        />
                                    </Grid>
                                    <Grid item container justify="center" alignItems="center" xs={2}>
                                        to
                                    </Grid>
                                    <Grid item xs={5}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            format="MM/dd/yyyy"
                                            margin="normal"
                                            id="endDate"
                                            label="End date"
                                            value={values.startDate}
                                            onChange={handleEndDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                        />
                                    </Grid>
                                </MuiPickersUtilsProvider>


                            </Grid>

                        ))}

                        <Grid item xs={12}>
                            <IconButton onClick={() => arrayHelpers.push({
                                level: '',
                                gpa: ''
                            })}
                                        aria-label="add"
                                        className={classes.margin}>
                                <AddIcon fontSize="large"/>
                            </IconButton>
                            <IconButton onClick={() => arrayHelpers.remove(values.educations.length - 1)}
                                        aria-label="delete"
                                        className={classes.margin}>
                                <DeleteIcon fontSize="large"/>
                            </IconButton>
                        </Grid>
                    </div>
                )}
            />

            <Divider/>
        </React.Fragment>
    );
}

const top100Films = [
    { title: 'The Shawshank Redemption', year: 1994 },
    { title: 'The Godfather', year: 1972 },
    { title: 'The Godfather: Part II', year: 1974 },
    { title: 'The Dark Knight', year: 2008 },
    { title: '12 Angry Men', year: 1957 },
    { title: "Schindler's List", year: 1993 },
    { title: 'Pulp Fiction', year: 1994 },
    { title: 'The Lord of the Rings: The Return of the King', year: 2003 },
    { title: 'The Good, the Bad and the Ugly', year: 1966 },
    { title: 'Fight Club', year: 1999 },
    { title: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
    { title: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
    { title: 'Forrest Gump', year: 1994 },
    { title: 'Inception', year: 2010 },
    { title: 'The Lord of the Rings: The Two Towers', year: 2002 },
    { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
    { title: 'Goodfellas', year: 1990 },
    { title: 'The Matrix', year: 1999 },
    { title: 'Seven Samurai', year: 1954 },
    { title: 'Star Wars: Episode IV - A New Hope', year: 1977 },
    { title: 'City of God', year: 2002 },
    { title: 'Se7en', year: 1995 },
    { title: 'The Silence of the Lambs', year: 1991 },
    { title: "It's a Wonderful Life", year: 1946 },
    { title: 'Life Is Beautiful', year: 1997 },
    { title: 'The Usual Suspects', year: 1995 },
    { title: 'Léon: The Professional', year: 1994 },
    { title: 'Spirited Away', year: 2001 },
    { title: 'Saving Private Ryan', year: 1998 },
    { title: 'Once Upon a Time in the West', year: 1968 },
    { title: 'American History X', year: 1998 },
    { title: 'Interstellar', year: 2014 },
    { title: 'Casablanca', year: 1942 },
    { title: 'City Lights', year: 1931 },
    { title: 'Psycho', year: 1960 },
    { title: 'The Green Mile', year: 1999 },
    { title: 'The Intouchables', year: 2011 },
    { title: 'Modern Times', year: 1936 },
    { title: 'Raiders of the Lost Ark', year: 1981 },
    { title: 'Rear Window', year: 1954 },
    { title: 'The Pianist', year: 2002 },
    { title: 'The Departed', year: 2006 },
    { title: 'Terminator 2: Judgment Day', year: 1991 },
    { title: 'Back to the Future', year: 1985 },
    { title: 'Whiplash', year: 2014 },
    { title: 'Gladiator', year: 2000 },
    { title: 'Memento', year: 2000 },
    { title: 'The Prestige', year: 2006 },
    { title: 'The Lion King', year: 1994 },
    { title: 'Apocalypse Now', year: 1979 },
    { title: 'Alien', year: 1979 },
    { title: 'Sunset Boulevard', year: 1950 },
    { title: 'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb', year: 1964 },
    { title: 'The Great Dictator', year: 1940 },
    { title: 'Cinema Paradiso', year: 1988 },
    { title: 'The Lives of Others', year: 2006 },
    { title: 'Grave of the Fireflies', year: 1988 },
    { title: 'Paths of Glory', year: 1957 },
    { title: 'Django Unchained', year: 2012 },
    { title: 'The Shining', year: 1980 },
    { title: 'WALL·E', year: 2008 },
    { title: 'American Beauty', year: 1999 },
    { title: 'The Dark Knight Rises', year: 2012 },
    { title: 'Princess Mononoke', year: 1997 },
    { title: 'Aliens', year: 1986 },
    { title: 'Oldboy', year: 2003 },
    { title: 'Once Upon a Time in America', year: 1984 },
    { title: 'Witness for the Prosecution', year: 1957 },
    { title: 'Das Boot', year: 1981 },
    { title: 'Citizen Kane', year: 1941 },
    { title: 'North by Northwest', year: 1959 },
    { title: 'Vertigo', year: 1958 },
    { title: 'Star Wars: Episode VI - Return of the Jedi', year: 1983 },
    { title: 'Reservoir Dogs', year: 1992 },
    { title: 'Braveheart', year: 1995 },
    { title: 'M', year: 1931 },
    { title: 'Requiem for a Dream', year: 2000 },
    { title: 'Amélie', year: 2001 },
    { title: 'A Clockwork Orange', year: 1971 },
    { title: 'Like Stars on Earth', year: 2007 },
    { title: 'Taxi Driver', year: 1976 },
    { title: 'Lawrence of Arabia', year: 1962 },
    { title: 'Double Indemnity', year: 1944 },
    { title: 'Eternal Sunshine of the Spotless Mind', year: 2004 },
    { title: 'Amadeus', year: 1984 },
    { title: 'To Kill a Mockingbird', year: 1962 },
    { title: 'Toy Story 3', year: 2010 },
    { title: 'Logan', year: 2017 },
    { title: 'Full Metal Jacket', year: 1987 },
    { title: 'Dangal', year: 2016 },
    { title: 'The Sting', year: 1973 },
    { title: '2001: A Space Odyssey', year: 1968 },
    { title: "Singin' in the Rain", year: 1952 },
    { title: 'Toy Story', year: 1995 },
    { title: 'Bicycle Thieves', year: 1948 },
    { title: 'The Kid', year: 1921 },
    { title: 'Inglourious Basterds', year: 2009 },
    { title: 'Snatch', year: 2000 },
    { title: '3 Idiots', year: 2009 },
    { title: 'Monty Python and the Holy Grail', year: 1975 },
];

//TODO: Add button that adds more experience fields on each click. Experience/job fields should be its own component.