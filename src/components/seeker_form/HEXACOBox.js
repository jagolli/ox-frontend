import React from "react";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from "@material-ui/core/Divider";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle'
import HEXACOForm from "../hexaco/HEXACOForm";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));

export default function HEXACOBox() {

    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Grid container justify="center" alignContent="center" spacing={3}>
                <Button variant="contained" color="primary" onClick={handleClickOpen}>
                    BEGIN TEST
                </Button>
                <Dialog maxWidth="md" open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">HEXACO</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <HEXACOForm/>
                        </DialogContentText>
                    </DialogContent>
                    <Divider/>
                    <DialogActions>
                        <Button onClick={handleClose} color="textPrimary">
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        </React.Fragment>
    );
}