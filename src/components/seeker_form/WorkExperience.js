import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

const marks = [
    {
        value: 1,
        label: '1',
    },
    {
        value: 2,
        label: '2',
    },
    {
        value: 3,
        label: '3',
    },
    {
        value: 4,
        label: '4',
    },
    {
        value: 5,
        label: '5',
    },
];


export default function WorkExperience() {

    const classes = useStyles();

    const [disabled, setDisabled] = React.useState(false);

    const [values, setValues] = React.useState({
        yrsExperience: '',
        startDate: new Date(),
        endDate: new Date(),
    });

    const handleDisabled = () => {
        setDisabled(!disabled);
    };

    const handleChange = (prop) => (event) => {
        setValues({...values, [prop]: event.target.value});
    };

    const handleStartDateChange = (date) => {
        setValues({...values, ["startDate"]: date});
    };

    const handleEndDateChange = (date) => {
        setValues({...values, ["endDate"]: date});
    };

    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <TextField
                        required
                        name="jobTitle"
                        label="Job title"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6} sm={6}>
                    <TextField
                        id="company"
                        name="company"
                        label="Company name"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6} sm={6}>
                    <TextField
                        required
                        id="city"
                        name="city"
                        label="City"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="state" name="state" label="State/Province/Region" fullWidth/>
                </Grid>
                <Grid item>
                    <FormControlLabel
                        onChange={handleDisabled}
                        control={<Checkbox color="primary" name="currentJob" value="yes"/>}
                        label="I am currently employed here"
                    />
                </Grid>

                <Grid item xs={6} sm={6}/>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid justify="flex-end" item container xs={5}>
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="startDate"
                            label="Start date"
                            value={values.startDate}
                            onChange={handleStartDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </Grid>
                    <Grid item container justify="center" alignItems="center" xs={2}>
                        to
                    </Grid>
                    <Grid item xs={5}>
                        <KeyboardDatePicker
                            disabled={disabled}
                            disableToolbar
                            variant="inline"
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="endDate"
                            label="End date"
                            value={values.startDate}
                            onChange={handleEndDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </Grid>
                </MuiPickersUtilsProvider>
                <Grid item xs={12}>
                    <Typography gutterBottom>
                        Rate your job satisfaction.
                    </Typography>
                </Grid>
                <Grid item xs={2}/>
                <Grid item xs={8}>
                    <Slider
                        defaultValue={3}
                        aria-labelledby="discrete-slider-restrict"
                        step={1}
                        min={1}
                        max={5}
                        valueLabelDisplay="auto"
                        marks={marks}
                    />
                </Grid>

            </Grid>
        </React.Fragment>
    );
}

