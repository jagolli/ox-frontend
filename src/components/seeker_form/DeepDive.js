import React from "react";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from "@material-ui/core/TextField";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Box from '@material-ui/core/Box';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    question: {
        margin: 'auto',
    },
    numField: {
        width: 115,
    }
}));

export default function DeepDive() {

    const classes = useStyles();

    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>Do you follow politics closely?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div className={classes.question}>
                                <FormControl component="fieldset">
                                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                                        <FormControlLabel
                                            value="yes"
                                            control={<Radio color="primary"/>}
                                            label="Yes"
                                            labelPlacement="start"
                                        />
                                        <FormControlLabel
                                            value="no"
                                            control={<Radio color="primary"/>}
                                            label="No"
                                            labelPlacement="start"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </div>
                        </AccordionDetails>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>Would you want to be a politician?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div className={classes.question}>
                                <FormControl component="fieldset">
                                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                                        <FormControlLabel
                                            value="yes"
                                            control={<Radio color="primary"/>}
                                            label="Yes"
                                            labelPlacement="start"
                                        />
                                        <FormControlLabel
                                            value="no"
                                            control={<Radio color="primary"/>}
                                            label="No"
                                            labelPlacement="start"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </div>
                        </AccordionDetails>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>How many close friends do you have?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div className={classes.question}>
                                <TextField
                                    className={classes.numField}
                                    required
                                    id="q3"
                                    type="number"
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                        </AccordionDetails>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>Do you frequent social media?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div className={classes.question}>
                                <FormControl component="fieldset">
                                    <RadioGroup row aria-label="position" name="position" defaultValue="top">
                                        <FormControlLabel
                                            value="yes"
                                            control={<Radio color="primary"/>}
                                            label="Yes"
                                            labelPlacement="start"
                                        />
                                        <FormControlLabel
                                            value="no"
                                            control={<Radio color="primary"/>}
                                            label="No"
                                            labelPlacement="start"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </div>
                        </AccordionDetails>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>What are your career goals?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <TextField
                                required
                                id="question1"
                                name="question1"
                                label=""
                                multiline
                                rowsMax={4}
                                fullWidth
                            />
                        </AccordionDetails>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>What is the most important thing in your
                                life?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <TextField
                                required
                                id="question1"
                                name="question1"
                                label=""
                                multiline
                                rowsMax={4}
                                fullWidth
                            />
                        </AccordionDetails>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>Do you believe that life has meaning?</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <TextField
                                required
                                id="question1"
                                name="question1"
                                label=""
                                multiline
                                rowsMax={4}
                                fullWidth
                            />
                        </AccordionDetails>
                    </Accordion>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}