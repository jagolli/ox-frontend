import React, {useState, useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Field} from 'formik';
import {TextField} from 'formik-material-ui';
import TextFieldOriginal from '@material-ui/core/TextField';
import axios from 'axios';
import Autocomplete from '@material-ui/lab/Autocomplete';

function sleep(delay = 0) {
    return new Promise(resolve => {
        setTimeout(resolve, delay);
    });
}

export default function UserDetails(props) {

    // const {register,errors,setValue} = {...props};
    // // const {register, handleSubmit, errors, setValue} = useForm();
    const {values, touched, errors, states, setStates, countries, setCountries, cities, setCities, setFieldValue} = {...props};
    const [open, setOpen] = React.useState({
        country: false,
        city: false,
        state: false,
    });



    async function getStates() {
        let response = await axios.get('http://127.0.0.1:8000/states/?country__name=' + values.country);
        let data = await response.data;
        setStates(data);
    }

    async function getCountries() {
        let response = await axios.get('http://127.0.0.1:8000/countries/');
        let data = await response.data;
        setCountries(data.map(country => country.name));
    }

    async function getCities() {
        let response = await axios.get('http://127.0.0.1:8000/cities/?country=1&region__name=' + values.state);
        let data = await response.data;
        setCities(data);
    }

    const loading ={
        city: open["city"] && cities.length === 0,
        state: open["state"] && states.length === 0,
    };

    useEffect(() => {


        if (open["country"]) {
            getCountries();
        }
        else {
            setCountries([]);
        }

        if (open["country"]) {

            getStates();
        }
        else {
            setStates([]);
        }

        if (open["country"]) {
            getCountries();
        }
        else {
            setCountries([]);
        }

        getCities();

    }, [open]);

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                User Details
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <Field
                        name="firstName"
                        component={TextField}
                        label="First name"
                        fullWidth
                        autoComplete="given-name"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Field
                        name="lastName"
                        component={TextField}
                        label="Last name"
                        fullWidth
                        autoComplete="family-name"
                    />
                </Grid>
                <Grid item xs={12}>
                    <Field
                        name="address1"
                        component={TextField}
                        label="Address line 1"
                        fullWidth
                        autoComplete="shipping address-line1"
                    />
                </Grid>
                <Grid item xs={12}>
                    <Field
                        name="address2"
                        component={TextField}
                        label="Address line 2"
                        fullWidth
                        autoComplete="shipping address-line2"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Autocomplete
                        freeSolo
                        name="country"
                        onOpen={() => {
                            setOpen({...open, ["country"]: true});
                        }}
                        onClose={() => {
                            setOpen({...open, ["country"]: false});
                        }}
                        loading={loading["country"]}
                        options={countries}
                        value={values.country}
                        onChange={(e, value) => setFieldValue("country", value)}
                        fullWidth
                        autoComplete="shipping country"
                        renderInput={(params) => (
                            <TextFieldOriginal
                                {...params}
                                error={errors['country']}
                                helperText={errors['country']}
                                name="country"
                                label="Country"
                                autoComplete="shipping country"
                            />
                        )}
                    />

                </Grid>
                <Grid item xs={12} sm={6}>
                    <Autocomplete
                        freeSolo
                        name="state"
                        options={states.map(state => state.name)}
                        value={values.state}
                        onChange={(e, value) => setFieldValue("state", value)}
                        fullWidth
                        renderInput={(params) => (
                            <TextFieldOriginal
                                {...params}
                                error={errors['state']}
                                helperText={errors['state']}
                                label="State/Province/Region"
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Autocomplete
                        freeSolo
                        name="city"
                        options={cities.map(city => city.name)}
                        value={values.city}
                        onChange={(e, value) => setFieldValue("city", value)}
                        fullWidth
                        autoComplete="shipping address-level2"
                        getOptionLabel={(option) => option}
                        renderInput={(params) => (
                            <TextFieldOriginal
                                {...params}
                                error={errors['city']}
                                helperText={errors['city']}
                                label="City"
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Field
                        component={TextField}
                        name="zip"
                        label="Zip / Postal code"
                        fullWidth
                        autoComplete="shipping postal-code"
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );

}

//TODO get max length from backend