import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import UserDetails from './UserDetails';
import Experience from "./Experience";
import Review from './Review';
import DeepDive from './DeepDive';
import HEXACOBox from "./HEXACOBox";
import axios from 'axios';
import {Form, Formik} from 'formik';
import * as Yup from "yup";


const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        width: 'auto',
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
}));

const steps = ['User Details', 'Experience', 'Deep Dive', 'HEXACO', 'Review'];

function getStepContent(step,values,touched,errors,states,setStates,countries,setCountries,cities,setCities,
                        setFieldValue,setValues) {
    switch (step) {
        case 0:
            return <UserDetails
                values={values}
                touched={touched}
                errors={errors}
                states={states}
                setStates={setStates}
                countries={countries}
                setCountries={setCountries}
                cities={cities}
                setCities={setCities}
                setFieldValue={setFieldValue}
                setValues={setValues}
            />;
        case 1:
            return <Experience
                values={values}
                errors={errors}
                states={states}
                setStates={setStates}
                countries={countries}
                cities={cities}
                setCities={setCities}
                setFieldValue={setFieldValue}
                setValues={setValues}
            />;
        case 2:
            return <DeepDive/>;
        case 3:
            return <HEXACOBox/>;
        case 4:
            return <Review/>;
        default:
            throw new Error('Unknown step');
    }
}

export default function SeekerForm() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const isLastStep = (activeStep === steps.length - 1);
    const [states, setStates] = useState([]);
    const [countries, setCountries] = useState([]);
    const [cities, setCities] = useState([]);

    const handleNext = () => {
        setActiveStep(activeStep + 1);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

    function getValidationSchema(step) {
        switch (step) {
            case 0:
                return Yup.object({
                    firstName: Yup.string()
                        .matches(/^[\-a-zA-Z ]+$/,'First name should only contain characters')
                        .max(30, 'Must be 30 characters or less')
                        .required('Required'),
                    lastName: Yup.string()
                        .matches(/^[\-a-zA-Z ]+$/,'Last name should only contain characters')
                        .max(30, 'Must be 30 characters or less')
                        .required('Required'),
                    address1: Yup.string()
                        .required('Required'),
                    address2: Yup.string(),
                    city: Yup.string()
                        .nullable()
                        .required('Required'),
                    state: Yup.string()
                        .nullable()
                        .required('Required'),
                    zip: Yup.number()
                        .required('Required'),
                    country: Yup.string()
                        .nullable()
                        .required('Required')
                });
            case 1:
                return Yup.object().shape({
                    educations: Yup.array()
                        .of(
                            Yup.object().shape({
                                level: Yup.string()
                                    .required('Required'),
                                gpa: Yup.number()
                                    .max(4.0, 'Max of 4.0')
                                    .positive('Must be positive')
                                    .required('Required'),
                                fieldOfStudy: Yup.string()
                                    .required('Required'),
                                school: Yup.string()
                                    .required('Required'),
                                city: Yup.string()
                                    .required('Required'),
                                state: Yup.string()
                                    .required('Required'),
                                zip: Yup.number()
                                    .required('Required'),
                                country: Yup.string()
                                    .required('Required')
                            })
                        )
                        .required('Must have education') // these constraints are shown if and only if inner constraints are satisfied
                        .min(2, 'Minimum of 2 education'),
                });
        }
    }

    const initialValues = {
        firstName: 'Jack',
        lastName: 'Agolli',
        address1: '523 Madison Park Dr',
        address2: '',
        city: 'Madison',
        state: 'Alabama',
        zip: '35758',
        country: '',
        educations: [{
            level:'',
            gpa:'',
            fieldOfStudy: '',
            school: '',
            city: '',
            state: '',
            zip: '',
            country: '',
        }],

    };


    return (
        <React.Fragment>
            <CssBaseline/>
            <Formik
                initialValues={initialValues}
                onSubmit={(values, {setSubmitting}) => {
                    if (!isLastStep) {
                        setSubmitting(false);
                        handleNext();
                        return;
                    }
                    // post data here if it is the last step
                    setTimeout(() => {
                        setSubmitting(false);
                        axios.post('http://127.0.0.1:8000/api/trajectory/', values
                        ).then(function (response) {
                            console.log(response);
                        }).catch(function (error) {
                            console.log(error);
                        });
                        // alert(JSON.stringify(values, null, 2));
                    }, 500);

                }}
                validationSchema={getValidationSchema(activeStep)}
            >
                {({setFieldValue ,setValues, errors,
                      touched, values}) => (
                    <>
                        <Form>
                            <main className={classes.layout}>
                                <Paper className={classes.paper}>
                                    <Stepper activeStep={activeStep} className={classes.stepper}>
                                        {steps.map((label) => (
                                            <Step key={label}>
                                                <StepLabel>{label}</StepLabel>
                                            </Step>
                                        ))}
                                    </Stepper>
                                    <React.Fragment>
                                        {activeStep === steps.length ? (
                                            <React.Fragment>
                                                <Typography variant="h5" gutterBottom>
                                                    Thank you for your order.
                                                </Typography>
                                                <Typography variant="subtitle1">
                                                    Your order number is #2001539. We have emailed your order
                                                    confirmation, and will
                                                    send you an update when your order has shipped.
                                                </Typography>
                                            </React.Fragment>
                                        ) : (
                                            <React.Fragment>
                                                {getStepContent(activeStep,values,touched,errors,states,setStates,
                                                    countries,setCountries,cities,setCities,setFieldValue,setValues)}
                                                <div className={classes.buttons}>

                                                    {activeStep !== 0 && (
                                                        <Button onClick={handleBack}
                                                                className={classes.button}>
                                                            Back
                                                        </Button>
                                                    )}
                                                    <Button
                                                        variant="contained"
                                                        color="primary"
                                                        type="submit"
                                                        className={classes.button}
                                                    >
                                                        {activeStep === steps.length - 1 ? 'Save' : 'Next'}
                                                    </Button>
                                                </div>
                                            </React.Fragment>
                                        )}
                                    </React.Fragment>
                                </Paper>
                            </main>
                        </Form>
                        <pre>{JSON.stringify(values, null, 2)}</pre>
                    </>
                )}
            </Formik>
        </React.Fragment>
    );
}