import React from 'react';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import WorkExperience from "./WorkExperience";
import EducationExperience from "./EducationExperience";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import {v1 as id} from "uuid";
import Chip from '@material-ui/core/Chip';
import SchoolIcon from '@material-ui/icons/School';
import WorkIcon from '@material-ui/icons/Work';
import UserDetails from "./UserDetails";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 300,
    },
    section: {
        marginTop: theme.spacing(2),
    },
    margin: {
        margin: theme.spacing(1),
    },
    divider: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    }
}));

export default function Experience(props) {

    const classes = useStyles();

    const {values, errors, states, setStates, countries, cities, setCities, setFieldValue, setValues} = {...props};

    const [jobs, setJobs] = React.useState([]);

    const handleAddJob = () => {
        setJobs([...jobs, {id: id()}]);
    };

    const handleRemoveJob = () => {
        setJobs(jobs.slice(0, -1));
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterTop gutterBottom>
                Previous education
            </Typography>
            <Chip icon={<SchoolIcon/>} label="Education 1"/>
            <EducationExperience
                values={values}
                errors={errors}
                states={states}
                setStates={setStates}
                countries={countries}
                cities={cities}
                setCities={setCities}
                setFieldValue={setFieldValue}
                setValues={setValues}
            />

            <Divider/>
            <div className={classes.section}>
                <Typography variant="h6" gutterTop gutterBottom>
                    Previous experience
                </Typography>
                <Chip icon={<WorkIcon/>} label="Job 1"/>
                <WorkExperience/>
            </div>

            {jobs.map((item, index) => (
                <div className={classes.section}>
                    <Divider className={classes.divider}/>
                    <Chip icon={<WorkIcon/>} label={"Job " + (index + 2)}/>
                    <WorkExperience/>
                </div>
            ))}
            <Grid item xs={12}>
                <IconButton onClick={handleAddJob} aria-label="delete" className={classes.margin}>
                    <AddIcon fontSize="large"/>
                </IconButton>
                <IconButton onClick={handleRemoveJob} aria-label="delete" className={classes.margin}>
                    <DeleteIcon fontSize="large"/>
                </IconButton>
            </Grid>
        </React.Fragment>
    );
}